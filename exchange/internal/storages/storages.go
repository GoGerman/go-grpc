package storages

import (
	"gitlab.com/GoGerman/go-grpc/exchange/internal/db/adapter"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/infrastructure/cache"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/modules/worker/repository"
	//	"gitlab.com/GoGerman/go-gRPC/exchange/internal/infrastructure/cache"
	//vstorage "gitlab.com/GoGerman/go-gRPC/exchange/internal/modules/auth/storage"
	//	ustorage "gitlab.com/GoGerman/go-gRPC/exchange/internal/modules/user/storage"
)

type Storages struct {
	Worker repository.WorkerRepositorier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Worker: repository.NewWorkerRepository(sqlAdapter),
	}
}
