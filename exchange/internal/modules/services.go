package modules

import (
	"gitlab.com/GoGerman/go-grpc/exchange/internal/infrastructure/component"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/infrastructure/server"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/modules/worker/service"

	"gitlab.com/GoGerman/go-grpc/exchange/internal/storages"
)

type Services struct {
	//User   uservice.Userer
	Worker service.WorkerServicer
	Server *server.ServerJSONRPC
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	c := server.NewJSONRPC(components.Conf.RPCServer, components.Conf.UserRPC, nil, components.Logger)
	//repoClientRPC := service.NewUserServiceJSONRPC(c.Client)
	workerService := service.NewWorkerService(storages.Worker, components.Logger)

	return &Services{
		//User:   userClientRPC,
		//Auth:   aservice.NewAuth(userClientRPC, storages.Verify, components),
		//Server: c,
		Server: c,
		Worker: workerService,
	}
}
