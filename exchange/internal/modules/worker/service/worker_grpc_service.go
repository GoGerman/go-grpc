package service

import (
	"context"
	"gitlab.com/GoGerman/go-grpc/exchange/gRPC/proto"
	"google.golang.org/protobuf/types/known/emptypb"
	"strconv"
)

type CryptoServiceGRPC struct {
	proto.UnimplementedExchServiceRPCServer
	service WorkerServicer
}

func NewExchServiceGRPC(service WorkerServicer) *CryptoServiceGRPC {
	return &CryptoServiceGRPC{service: service}
}

func (c CryptoServiceGRPC) History(ctx context.Context, e *emptypb.Empty) (*proto.HistoryResponse, error) {
	out := c.service.HistoryService(ctx)
	res := make([]*proto.QueryValue, len(out.Coins))
	for i := range out.Coins {
		res[i] = &proto.QueryValue{
			Id:        int64(out.Coins[i].ID),
			Name:      out.Coins[i].Name,
			LastTrade: out.Coins[i].LastTrade,
		}
	}
	return &proto.HistoryResponse{
		Coins: res,
	}, nil
}

func (c CryptoServiceGRPC) Max(ctx context.Context, e *emptypb.Empty) (*proto.MaxResponse, error) {
	out := c.service.MaxService(ctx)
	res := make([]*proto.QueryValue, len(out.Coins))
	for i := range out.Coins {
		high, _ := strconv.ParseFloat(out.Coins[i].High, 64)
		res[i] = &proto.QueryValue{
			Id:   int64(out.Coins[i].ID),
			Name: out.Coins[i].Name,
			High: high,
		}
	}
	return &proto.MaxResponse{
		Coins: res,
	}, nil
}

func (c CryptoServiceGRPC) Min(ctx context.Context, e *emptypb.Empty) (*proto.MinResponse, error) {
	out := c.service.MinService(ctx)
	res := make([]*proto.QueryValue, len(out.Coins))
	for i := range out.Coins {
		low, _ := strconv.ParseFloat(out.Coins[i].Low, 64)
		res[i] = &proto.QueryValue{
			Id:   int64(out.Coins[i].ID),
			Name: out.Coins[i].Name,
			Low:  low,
		}
	}
	return &proto.MinResponse{
		Coins: res,
	}, nil
}

func (c CryptoServiceGRPC) Avg(ctx context.Context, e *emptypb.Empty) (*proto.AvgResponse, error) {
	out := c.service.AvgService(ctx)
	res := make([]*proto.QueryValue, len(out.Coins))
	for i := range out.Coins {
		res[i] = &proto.QueryValue{
			Id:   int64(out.Coins[i].ID),
			Name: out.Coins[i].Name,
			Avg:  out.Coins[i].Avg,
		}
	}
	return &proto.AvgResponse{
		Coins: res,
	}, nil
}
