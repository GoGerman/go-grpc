package service

import (
	"context"
	"github.com/streadway/amqp"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/models"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/modules/worker/repository"
	"go.uber.org/zap"
)

type WorkerService struct {
	storage repository.WorkerRepositorier
	logger  *zap.Logger
}

func NewWorkerService(storage repository.WorkerRepositorier, logger *zap.Logger) *WorkerService {
	return &WorkerService{
		storage: storage,
		logger:  logger,
	}
}

func (w *WorkerService) WriteResponse(ctx context.Context, entity []models.QueryValue, q amqp.Queue, ch *amqp.Channel) PairsCreateOut {
	err := w.storage.WriteResponseDB(ctx, entity, q, ch)
	if err != nil {
		return PairsCreateOut{
			Message:   "can't create",
			ErrorCode: 1,
		}
	}

	return PairsCreateOut{
		Message:   "created",
		ErrorCode: 0,
	}
}

func (w *WorkerService) MaxService(ctx context.Context) models.MaxOut {
	return w.storage.Max(ctx)
}

func (w *WorkerService) MinService(ctx context.Context) models.MinOut {
	return w.storage.Min(ctx)
}

func (w *WorkerService) AvgService(ctx context.Context) models.AvgOut {
	return w.storage.Avg(ctx)
}

func (w *WorkerService) HistoryService(ctx context.Context) models.HistoryOut {
	return w.storage.History(ctx)
}
