package service

import (
	"context"
	"github.com/streadway/amqp"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/models"
)

type WorkerServicer interface {
	WriteResponse(ctx context.Context, entity []models.QueryValue, q amqp.Queue, ch *amqp.Channel) PairsCreateOut
	MaxService(ctx context.Context) models.MaxOut
	MinService(ctx context.Context) models.MinOut
	AvgService(ctx context.Context) models.AvgOut
	HistoryService(ctx context.Context) models.HistoryOut
}

type PairsCreateOut struct {
	Message   string `json:"message"`
	ErrorCode int    `json:"error_code"`
}
