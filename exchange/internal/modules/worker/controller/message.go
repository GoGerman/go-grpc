package controller

import "gitlab.com/GoGerman/go-grpc/exchange/internal/models"

type ExchangeResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code"`
	Data      Data `json:"data"`
}

type Data struct {
	Message string              `json:"message"`
	Crypto  []models.QueryValue `json:"crypto"`
}
