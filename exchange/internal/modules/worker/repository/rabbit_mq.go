package repository

import (
	"github.com/streadway/amqp"
	"log"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func RabbitMqInit() (amqp.Queue, *amqp.Channel) {
	conn, err := amqp.Dial("amqp://guest:guest@rabbitexch:5672/")
	failOnError(err, `rabbitmq: dial err`)

	ch, err := conn.Channel()
	failOnError(err, `rabbitmq: channel err`)

	log.Println(`successfully connected to RabbitMQ instance`)

	q, err := ch.QueueDeclare(
		`exchange`,
		false,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, `rabbitmq: queue declare err`)

	return q, ch
}

func SendMessageRabbitMQ(message string, q amqp.Queue, ch *amqp.Channel) error {
	err := ch.Publish(
		``,
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(message),
		})
	failOnError(err, `rabbitmq: publish err`)

	log.Println(`successfully published message to queue`)
	return nil
}
