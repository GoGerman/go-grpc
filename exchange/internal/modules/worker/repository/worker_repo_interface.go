package repository

import (
	"context"
	"github.com/streadway/amqp"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/models"
)

type WorkerRepositorier interface {
	WriteResponseDB(ctx context.Context, entity []models.QueryValue, q amqp.Queue, ch *amqp.Channel) error
	Max(ctx context.Context) models.MaxOut
	Min(ctx context.Context) models.MinOut
	Avg(ctx context.Context) models.AvgOut
	History(ctx context.Context) models.HistoryOut
}
