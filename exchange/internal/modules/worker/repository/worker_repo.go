package repository

import (
	"context"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/streadway/amqp"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/db/adapter"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/infrastructure/db/scanner"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/infrastructure/db/types"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/models"
	"log"
	"sort"
	"strconv"
	"time"
)

type WorkerRepository struct {
	sqlAdapter *adapter.SQLAdapter
}

func NewWorkerRepository(sqlAdapter *adapter.SQLAdapter) WorkerRepositorier {
	return &WorkerRepository{sqlAdapter: sqlAdapter}
}

func (w *WorkerRepository) WriteResponseDB(ctx context.Context, entity []models.QueryValue, q amqp.Queue, ch *amqp.Channel) error {
	var maxDto models.MaxDTO
	var minDto models.MinDTO

	var listMax []models.MaxDTO
	var listMin []models.MinDTO

	sort.Slice(entity, func(i, j int) bool {
		return entity[i].Name < entity[j].Name
	})
	sort.Slice(listMax, func(i, j int) bool {
		return listMax[i].Name.String < listMax[j].Name.String
	})
	sort.Slice(listMin, func(i, j int) bool {
		return listMin[i].Name.String < listMin[j].Name.String
	})

	err := w.sqlAdapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from table %v`, maxDto)
	}

	err = w.sqlAdapter.List(ctx, &listMin, minDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from table %v`, minDto)
	}

	for i := range entity {
		max, _ := strconv.ParseFloat(entity[i].High, 64)
		min, _ := strconv.ParseFloat(entity[i].Low, 64)

		coinsMax := models.MaxDTO{
			ID:        i,
			Name:      types.NewNullString(entity[i].Name),
			BuyPrice:  types.NewNullString(entity[i].BuyPrice),
			SellPrice: types.NewNullString(entity[i].SellPrice),
			LastTrade: types.NewNullString(entity[i].LastTrade),
			High:      types.NewNullFloat64(max),
			Low:       types.NewNullFloat64(min),
			Avg:       types.NewNullString(entity[i].Avg),
			Vol:       types.NewNullString(entity[i].Vol),
			VolCurr:   types.NewNullString(entity[i].VolCurr),
			Updated:   entity[i].Updated,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		}
		coinsMin := models.MinDTO{
			ID:        i,
			Name:      types.NewNullString(entity[i].Name),
			BuyPrice:  types.NewNullString(entity[i].BuyPrice),
			SellPrice: types.NewNullString(entity[i].SellPrice),
			LastTrade: types.NewNullString(entity[i].LastTrade),
			High:      types.NewNullFloat64(max),
			Low:       types.NewNullFloat64(min),
			Avg:       types.NewNullString(entity[i].Avg),
			Vol:       types.NewNullString(entity[i].Vol),
			VolCurr:   types.NewNullString(entity[i].VolCurr),
			Updated:   entity[i].Updated,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		}

		if len(listMax) < 1 || len(listMin) < 1 {
			err = w.sqlAdapter.Create(ctx, &coinsMax)
			if err != nil {
				log.Fatalf(`error (1.1) when trying to create data at table %v`, maxDto)
			}
			err = w.sqlAdapter.Create(ctx, &coinsMin)
			if err != nil {
				log.Fatalf(`error (1.2) when trying to create data at table %v`, minDto)
			}
		} else {
			if listMax[i].High.Float64 < coinsMax.High.Float64 &&
				listMax[i].Name.String == coinsMax.Name.String { // попробовать решить проблему по ID
				err = w.sqlAdapter.Update(ctx, &coinsMax, adapter.Condition{
					Equal: sq.Eq{"name": coinsMax.Name},
				}, scanner.Update)
				if err != nil {
					log.Fatalf(`error (1.4) when trying to update data at table %v`, maxDto)
				}
				message := fmt.Sprintf("максимальная цена %s была %f, стала %f",
					coinsMax.Name.String, listMax[i].High.Float64, coinsMax.High.Float64)
				err = SendMessageRabbitMQ(message, q, ch)
				if err != nil {
					log.Fatalf(`error (1.3) when trying to send message at table %v`, maxDto)
				}
			}
			if listMin[i].Low.Float64 > coinsMin.Low.Float64 &&
				listMin[i].Name.String == coinsMin.Name.String {
				err = w.sqlAdapter.Update(ctx, &coinsMin, adapter.Condition{
					Equal: sq.Eq{"name": coinsMin.Name},
				}, scanner.Update)
				if err != nil {
					log.Fatalf(`error (1.4) when trying to update data at table %v`, minDto)
				}
				message := fmt.Sprintf("минимальная цена %s была %f, стала %f",
					coinsMin.Name.String, listMin[i].Low.Float64, coinsMin.Low.Float64)
				err = SendMessageRabbitMQ(message, q, ch)
				if err != nil {
					log.Fatalf(`error (1.3) when trying to send message at table %v`, minDto)
				}
			}
		}
	}
	return nil
}

func (w *WorkerRepository) Max(ctx context.Context) models.MaxOut {
	var maxDto models.MaxDTO
	var err error

	var listMax []models.MaxDTO
	err = w.sqlAdapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Max and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return models.MaxOut{
			ErrorCode: 1,
		}
	}
	var result []models.QueryValue
	for _, e := range listMax {
		result = append(result, models.QueryValue{
			ID:   e.ID,
			Name: e.Name.String,
			High: e.Name.String,
		})
	}

	return models.MaxOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (w *WorkerRepository) Min(ctx context.Context) models.MinOut {
	var minDto models.MinDTO
	var err error

	var listMin []models.MinDTO
	err = w.sqlAdapter.List(ctx, &listMin, minDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Min and table %v`, minDto)
	}
	if len(listMin) == 0 {
		return models.MinOut{
			ErrorCode: 1,
		}
	}
	var result []models.QueryValue
	for _, e := range listMin {
		result = append(result, models.QueryValue{
			ID:   e.ID,
			Name: e.Name.String,
			Low:  e.Name.String,
		})
	}

	return models.MinOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (w *WorkerRepository) Avg(ctx context.Context) models.AvgOut {
	var maxDto models.MaxDTO
	var err error

	var listMax []models.MaxDTO
	err = w.sqlAdapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method Avg and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return models.AvgOut{
			ErrorCode: 1,
		}
	}
	var result []models.QueryValue
	for _, e := range listMax {
		result = append(result, models.QueryValue{
			ID:   e.ID,
			Name: e.Name.String,
			Avg:  e.Avg.String,
		})
	}

	return models.AvgOut{
		Coins:     result,
		ErrorCode: 0,
	}
}

func (w *WorkerRepository) History(ctx context.Context) models.HistoryOut {
	var maxDto models.MaxDTO
	var err error

	var listMax []models.MaxDTO
	err = w.sqlAdapter.List(ctx, &listMax, maxDto.TableName(), adapter.Condition{})
	if err != nil {
		log.Fatalf(`error when trying to retrieve data from method History and table %v`, maxDto)
	}
	if len(listMax) == 0 {
		return models.HistoryOut{
			ErrorCode: 1,
		}
	}
	var result []models.QueryValue
	for _, e := range listMax {
		result = append(result, models.QueryValue{
			ID:        e.ID,
			Name:      e.Name.String,
			LastTrade: e.LastTrade.String,
		})
	}

	return models.HistoryOut{
		Coins:     result,
		ErrorCode: 0,
	}
}
