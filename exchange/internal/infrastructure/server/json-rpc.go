package server

import (
	"context"
	"fmt"
	"gitlab.com/GoGerman/go-grpc/exchange/config"
	"go.uber.org/zap"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type ServerJSONRPC struct {
	conf   config.RPCClient
	logger *zap.Logger
	srv    *rpc.Server
	Client *rpc.Client
	Server config.RPCServer
}

func NewJSONRPC(con config.RPCServer, conf config.RPCClient, jsonRPCServer *rpc.Server, logger *zap.Logger) *ServerJSONRPC {
	client, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", conf.Host, conf.Port))
	if err != nil {
		client = nil
	}

	return &ServerJSONRPC{conf: conf, Client: client, logger: logger, srv: jsonRPCServer, Server: con}
}

func (s *ServerJSONRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			s.logger.Error("json rpc server register error", zap.Error(err))
			chErr <- err
		}

		s.logger.Info("json rpc server started", zap.String("port", s.conf.Port))
		var conn net.Conn
		for {
			select {
			case <-ctx.Done():
				s.logger.Error("json rpc: stopping server")
				return
			default:
				var connErr error
				conn, connErr = l.Accept()
				if err != nil {
					s.logger.Error("json rpc: net tcp accept", zap.Error(connErr))
				}
				go s.srv.ServeCodec(jsonrpc.NewServerCodec(conn))
			}
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return err
}
