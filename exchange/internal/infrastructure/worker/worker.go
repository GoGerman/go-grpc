package worker

import (
	"context"
	"encoding/json"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/models"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/modules/worker/repository"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/modules/worker/service"
	"log"
	"net/http"
	"time"
)

type Worker struct {
	service service.WorkerServicer
	ctx     context.Context
}

func NewWorker(ctx context.Context, service service.WorkerServicer) *Worker {
	return &Worker{
		service: service,
		ctx:     ctx,
	}
}

func (w *Worker) WorkerRun() {
	ticker := time.NewTicker(10 * time.Second)
	done := make(chan struct{})

	q, ch := repository.RabbitMqInit()

	for {
		select {
		case <-ticker.C:
			response, err := http.Get("https://api.exmo.com/v1.1/ticker")
			if err != nil {
				log.Fatal(err)
			}
			m := make(models.Coins)
			err = json.NewDecoder(response.Body).Decode(&m)
			if err != nil {
				log.Fatal(err)
			}
			var queryValues []models.QueryValue
			for name, queryValue := range m {
				queryValue.Name = name
				queryValues = append(queryValues, queryValue)
			}

			w.service.WriteResponse(context.Background(), queryValues, q, ch)
		case <-done:
			return
		}
	}

}
