package models

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type Coins map[string]QueryValue

type QueryValue struct {
	ID        int
	Name      string
	BuyPrice  string `json:"buy_price"`
	SellPrice string `json:"sell_price"`
	LastTrade string `json:"last_trade"`
	High      string `json:"high"`
	Low       string `json:"low"`
	Avg       string `json:"avg"`
	Vol       string `json:"vol"`
	VolCurr   string `json:"vol_curr"`
	Updated   int    `json:"updated"`
}

func (q *QueryValue) TableName() string {
	return "queryValue"
}

func (q *QueryValue) OnCreate() []string {
	return []string{}
}

type AvgOut struct {
	Coins     []QueryValue `json:"coins"`
	ErrorCode int          `json:"error_code"`
}

type HistoryOut struct {
	Coins     []QueryValue `json:"coins"`
	ErrorCode int          `json:"error_code"`
}
