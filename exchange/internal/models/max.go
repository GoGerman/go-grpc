package models

import (
	"gitlab.com/GoGerman/go-grpc/exchange/internal/infrastructure/db/types"
	"time"
)

type MaxDTO struct {
	ID        int               `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name      types.NullString  `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	BuyPrice  types.NullString  `json:"buy_price" db:"buy_price" db_type:"varchar(200)" db_default:"default null"  db_ops:"create,update"`
	SellPrice types.NullString  `json:"sell_price" db:"sell_price" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	LastTrade types.NullString  `json:"last_trade" db:"last_trade" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	High      types.NullFloat64 `json:"high" db:"high" db_type:"numeric" db_default:"default 0" db_ops:"create,update"`
	Low       types.NullFloat64 `json:"low" db:"low" db_type:"numeric" db_default:"default 0" db_ops:"create,update"`
	Avg       types.NullString  `json:"avg" db:"avg" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	Vol       types.NullString  `json:"vol" db:"vol" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	VolCurr   types.NullString  `json:"vol_curr" db:"vol_curr" db_type:"varchar(200)" db_default:"default null" db_ops:"create,update"`
	Updated   int               `json:"updated" db:"updated" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	CreatedAt time.Time         `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_ops:"created_at"`
	UpdatedAt time.Time         `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_ops:"updated_at"`
	DeletedAt types.NullTime    `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_ops:"deleted_at"`
}

func (m *MaxDTO) TableName() string {
	return "max"
}

func (m *MaxDTO) OnCreate() []string {
	return []string{}
}

type MaxOut struct {
	Coins     []QueryValue `json:"coins"`
	ErrorCode int          `json:"error_code"`
}
