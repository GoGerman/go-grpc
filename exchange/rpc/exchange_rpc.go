package rpc

import (
	"context"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/models"
	"gitlab.com/GoGerman/go-grpc/exchange/internal/modules/worker/service"
)

type RpcClientExchange struct {
	service service.WorkerServicer
}

func NewClientRPCService(service service.WorkerServicer) *RpcClientExchange {
	return &RpcClientExchange{service: service}
}

func (r *RpcClientExchange) History(in struct{}, out *models.HistoryOut) error {
	*out = r.service.HistoryService(context.Background())
	return nil
}

func (r *RpcClientExchange) Max(in struct{}, out *models.MaxOut) error {
	*out = r.service.MaxService(context.Background())
	return nil
}

func (r *RpcClientExchange) Min(in struct{}, out *models.MinOut) error {
	*out = r.service.MinService(context.Background())
	return nil
}

func (r *RpcClientExchange) Avg(in struct{}, out *models.AvgOut) error {
	*out = r.service.AvgService(context.Background())
	return nil
}
